import Vue from 'vue'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

Vue.component('rich-editor', ClassicEditor)
