
import firebase from 'firebase'
import 'firebase/auth'

let firebaseConfig = {
  apiKey: 'AIzaSyCyv5vLZAwKdVEjJnQyhlx2Lv7rTrO6-Ac',
  authDomain: 'portfolio-blog-d846e.firebaseapp.com',
  databaseURL: 'https://portfolio-blog-d846e.firebaseio.com',
  projectId: 'portfolio-blog-d846e',
  storageBucket: "portfolio-blog-d846e.appspot.com",
  // messagingSenderId: '558494679493',
  // appId: '1:558494679493:web:920b6801f8902e76448bac',
  // measurementId: 'G-H3G2BTFP7D'
}

const fire = !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : null

export default fire
