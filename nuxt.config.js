export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  target: 'static',
  head: {
    title: 'Abiral Sangroula - Portfolio',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Material+Icons' }
    ],
    script: [
      // {src: './plugins/facebook.js'}
    ]
  },


  /*
   ** Customize the generated output folder
   */
  generate: {
    dir: 'public'
  },

  /*
  ** Customize the base url
  */
  router: {
    base: '/portfolio-blog/'
  },

  /*database*/
  database: {
    emulatorPort: 9000,
    emulatorHost: 'localhost'
  },


  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    'assets/scss/style.scss'
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    // 'plugins/firebase.js',
    { src: '~/plugins/firebase', mode: 'client'},
    { src: '~/plugins/pagination', mode: 'client'},
    { src: '~/plugins/facebook', mode: 'client'},
    { src: '~/plugins/ck.client.js'},
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [],

  // Modules (https://go.nuxtjs.dev/config-modules)

  modules: [
    [
      '@nuxtjs/component-cache',
      '@nuxtjs/axios',
      '@nuxtjs/firebase',
      // {
      //   config: {
      //     apiKey: 'AIzaSyCyv5vLZAwKdVEjJnQyhlx2Lv7rTrO6-Ac',
      //     authDomain: 'portfolio-blog-d846e.firebaseapp.com',
      //     databaseURL: 'https://portfolio-blog-d846e.firebaseio.com',
      //     projectId: 'portfolio-blog-d846e',
      //     storageBucket: 'portfolio-blog-d846e.appspot.com',
      //     messagingSenderId: '558494679493',
      //     appId: '1:558494679493:web:920b6801f8902e76448bac',
      //     measurementId: 'G-H3G2BTFP7D'
      //   },
      //   services: {
      //     realtimeDb: true,// this is the realtime database service
      //     database: true
      //   }
      // }
    ]
  ],


  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {}
}



